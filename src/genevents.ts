import {
    isResourceLike,
    getAllAttributes,
    isEvent,
    getKeyAttributes,
    AnyKind
} from "./treetypes"
import { BaseGen, Verbs } from "./genbase"
import { camelCase, snakeCase, getVersion } from "./names"
import { isPrimitiveType } from "./parse"

/**
 * generate swagger from the parsed representation
 */

// The more correct "publish OR subscribe" type was being fiddly,
// so I didn't bother for now.
interface Channel {
    description: string
    publish?: PublishOrSubscribe
    subscribe?: PublishOrSubscribe
}

interface PublishOrSubscribe {
    summary: string
    operationId: string
    message: {
        $ref: string
    }
}
type ChannelMap = { [name: string]: Channel }

export default class EventsGen extends BaseGen {
    public generate() {
        this.markGenerate()
        const messages: any = {}
        const schemas: any = {}
        const channels: ChannelMap = {}
        const headers: any = {}
        const asyncapi: object = {
            asyncapi: "2.0.0",
            info: {
                title: this.namespace.title,
                description: this.translateDoc(this.namespace.comment),
                version: this.namespace.version
            },
            servers: {
                production: {
                    url: "pubsub.liveramp.com:{port}",
                    protocol: "Google Cloud Pub/Sub",
                    description: "LiveRamp Production pubsub instance",
                    variables: {
                        port: {
                            description:
                                "Secure connection (TLS) is available through port 8883",
                            default: "1883"
                        }
                    }
                }
            },
            defaultContentType: "application/json",
            channels,
            components: {
                messages,
                schemas,
                messageTraits: headers
            }
        }

        // form channels
        this.formChannels(channels)

        // form messages
        const haveEvents = this.formMessages(messages)

        // model definitions
        const headerNames = this.formDefinitions(schemas)

        // add standard header definitions - tbd move this to a file
        this.addStandardHeaderDefinitions(headers, schemas)

        // lift the headers
        for (const name of headerNames) {
            this.liftToHeader(name, schemas, headers)
        }

        if (!haveEvents) {
            throw new Error("No events are listed in the specification")
        }
        return asyncapi
    }

    private formChannels(channels: ChannelMap) {
        // handle each primary structure and work out if we should generate structures for it
        for (const el of this.defs) {
            // don't generate for any imported def
            if (el.secondary) {
                continue
            }

            const unique = camelCase(this.formSingleUniqueName(el))
            if (
                isResourceLike(el) &&
                !el.future &&
                this.extractOp(el, "EVENTS")
            ) {
                channels[
                    "topics/" +
                        snakeCase(this.getSpace()) +
                        "_" +
                        getVersion(el.name) +
                        "-" +
                        snakeCase(el.name)
                ] = {
                    description: el.comment || "no documentation",
                    subscribe: {
                        summary: "REST: " + el.name,
                        operationId: el.name,
                        message: {
                            $ref: `#/components/messages/${unique}`
                        }
                    }
                }
            }
            if (isEvent(el)) {
                const psKey = el.produces ? "subscribe" : "publish"
                const details = {
                    description: el.comment || "no documentation",
                    [psKey]: {
                        summary: "Adhoc: " + el.name,
                        operationId: el.name,
                        message: {
                            $ref: `#/components/messages/${unique}`
                        }
                    }
                }

                channels[
                    "topics/" +
                        this.mainNamespace +
                        "_" +
                        getVersion(el.name) +
                        "-" +
                        snakeCase(el.name)
                ] = details
            }
        }
    }

    private addStandardHeaderDefinitions(headers: any, schemas: any) {
        headers.RestHeader = {
            headers: {
                type: "object",
                properties: {
                    verb: {
                        description: "",
                        $ref: "#/components/schemas/VerbEnum"
                    },
                    location: {
                        description: "",
                        type: "string",
                        format: "url",
                        example: "https://www.domain.com (url)"
                    },
                    ids: {
                        description: "",
                        items: {
                            type: "string"
                        },
                        type: "array"
                    }
                },
                required: ["verb", "location", "ids"]
            }
        }
        schemas.VerbEnum = {
            type: "string",
            enum: ["POST", "PUT", "PATCH", "GET", "MULTIGET", "DELETE"]
        }
    }

    private liftToHeader(name: string, schemas: any, headers: any) {
        const hdr = schemas[name]
        headers[name] = { headers: hdr }
    }

    private formMessages(messages: any) {
        // handle each primary structure and work out if we should generate structures for it
        let haveEvents = false
        for (const el of this.defs) {
            // don't generate for any imported def
            if (el.secondary) {
                continue
            }

            if (
                isResourceLike(el) &&
                !el.future &&
                this.extractOp(el, "EVENTS")
            ) {
                haveEvents = true
                const unique = camelCase(this.formSingleUniqueName(el))
                messages[unique] = {
                    name: unique,
                    title: unique,
                    summary: el.comment,
                    contentType: "application/json",
                    traits: [{ $ref: `#/components/messageTraits/RestHeader` }],
                    payload: { $ref: `#/components/schemas/${unique}Output` }
                }
            }
            if (isEvent(el)) {
                haveEvents = true
                const unique = camelCase(this.formSingleUniqueName(el))
                messages[unique] = {
                    name: unique,
                    title: unique,
                    summary: el.comment,
                    contentType: "application/json",
                    traits: [
                        { $ref: `#/components/messageTraits/${unique}Header` }
                    ],
                    payload: { $ref: `#/components/schemas/${unique}` }
                }
            }
        }
        return haveEvents
    }

    private formDefinitions(schemas: any) {
        const headers: string[] = []
        for (const def of this.defs) {
            if (def.generateInput) {
                switch (def.kind) {
                    case "resource-like":
                        if (!def.secondary) {
                            this.addResourceDefinition(
                                schemas,
                                def,
                                Verbs.GET,
                                "Output"
                            )
                        }
                        break
                    case "event":
                        // add the header and the payload
                        const called = this.addStructureDefinition(
                            schemas,
                            def,
                            "Header",
                            def.header || []
                        )
                        headers.push(called)
                        this.addStructureDefinition(
                            schemas,
                            def,
                            "",
                            getKeyAttributes(def)
                        )
                        break
                    case "structure":
                        this.addStructureDefinition(
                            schemas,
                            def,
                            "",
                            getKeyAttributes(def)
                        )
                        break
                    case "union":
                        this.addUnionDefinition(schemas, def, "")
                        break
                    case "enum":
                        this.addEnumDefinition(schemas, def, "")
                        break
                }
            }
        }
        return headers // to lift up
    }

    /** determine if we should generate definitions for each entity */
    private markGenerate() {
        // handle each primary structure and work out if we should generate structures for it
        const visited = new Set<string>()
        for (const el of this.defs) {
            if (isResourceLike(el) || isEvent(el)) {
                this.follow(el, visited, 0)
            }
        }
    }

    private follow(el: AnyKind, visited: Set<string>, level: number) {
        // have we seen this before?
        const unique = this.formSingleUniqueName(el)
        if (visited.has(unique)) {
            return
        }
        visited.add(unique)

        if (isResourceLike(el)) {
            if (el.future) {
                return
            }
            if (el.secondary && level === 0) {
                visited.delete(unique)
                return
            }
            if (!this.extractOp(el, "EVENTS") && level === 0) {
                visited.delete(unique)
                return
            }
        }

        el.generateInput = true

        // now work out if attributes reference any structures or other resources
        for (const attr of getAllAttributes(el) || []) {
            if (!isPrimitiveType(attr.type.name)) {
                const def = this.extractDefinition(attr.type.name)
                if (def && !attr.inline && !attr.linked) {
                    this.follow(def, visited, level + 1)
                }
            }
        }
    }
}
