// these are all the models we want to generate specs for, and to check
export const allModels = [
    "linked",
    "databuyer",
    "eventing",
    "dataset",
    "checkrules",
    "privacy",
    "optionality",
    "authorization",
    "complex-resource",
    "patchable",
    "direct2dist",
    "distribution",
    "file",
    "request",
    "simple-resource",
    "singleton",
    "stringmaps",
    "upversion",
    "multiplicity",
    "namespaces"
]
