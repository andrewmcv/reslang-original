openapi: 3.0.1
info:
  title: 'Consent file API - BETA '
  description: API for retrieving consent files
  version: 0.0.1
servers:
  - url: 'https://api.liveramp.com/privacy'
tags:
  - name: ConsentFile
    description: '(resource)  '
paths:
  /v1/consent-files:
    get:
      tags:
        - ConsentFile
      operationId: Get ConsentFiles
      description: ''
      responses:
        '200':
          description: ConsentFiles retrieved successfully
          headers:
            X-Total-Count:
              description: Total number of ConsentFiles returned by the query
              schema:
                type: integer
                format: int32
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ConsentFileMultiResponse'
      parameters:
        - in: query
          name: offset
          description: >-
            Offset of the ConsentFiles (starting from 0) to include in the
            response.
          schema:
            type: integer
            format: int32
            default: 0
            minimum: 0
        - in: query
          name: limit
          description: >-
            Number of ConsentFiles to return. If unspecified, 10 max will be
            returned. Maximum value for limit can be 100
          schema:
            type: integer
            format: int32
            default: 10
            minimum: 1
            maximum: 100
        - in: query
          name: createdSince
          required: false
          schema:
            type: string
            format: ISO8601 UTC date-time
components:
  parameters: {}
  schemas:
    ConsentFileOutput:
      type: object
      properties:
        id:
          type: string
        identifierType:
          allOf:
            - $ref: '#/components/schemas/IdentifierType'
          type: string
        requestType:
          allOf:
            - $ref: '#/components/schemas/RequestType'
          type: string
        dataController:
          allOf:
            - $ref: '#/components/schemas/DataController'
          type: object
        fileUrl:
          type: string
        timeStamp:
          type: string
          format: ISO8601 UTC date-time
          example: '2019-04-13T03:35:34Z'
      required:
        - id
        - identifierType
        - requestType
        - dataController
        - fileUrl
        - timeStamp
    ConsentFileMultiResponse:
      type: object
      properties:
        consentFiles:
          description: Array of retrieved ConsentFiles
          type: array
          items:
            $ref: '#/components/schemas/ConsentFileOutput'
    DataController:
      type: object
      properties:
        controllerSpecifier:
          type: string
        controllerIdentification:
          type: string
        liveRampControlled:
          type: boolean
      required:
        - controllerSpecifier
        - controllerIdentification
        - liveRampControlled
    RequestType:
      type: string
      enum:
        - OPT_OUT
        - DELETE
    IdentifierType:
      type: string
      enum:
        - IDENTITY_LINK
        - COOKIE
        - IDFA
        - AAID
        - MUID
        - CUSTOM_ID
    StandardError:
      type: object
      properties:
        httpStatus:
          description: HTTP error status code for this problem
          type: integer
          format: int32
        errorCode:
          description: 'Service specific error code, more granular'
          type: string
        message:
          description: 'General, human readable error message'
          type: string
      required:
        - httpStatus
        - errorCode
        - message

